﻿using UnityEngine;
using System.Collections;

public class Malos : DependesiaC
{

		public Malos ()
		{
				Create ();
		}

		public override void Create ()
		{
				textura2D = new FSprite ("Enemy_1_0");
				pocicion = new Vector2 (0, 0);
				textura2D.x = pocicion.x;
				textura2D.y = pocicion.y;
				AddChild (textura2D);
				ActualizarTex ();

		}

		public override void Actualizar30 ()
		{
				ActualizarTex ();							

				rectanguloColicion = textura2D.localRect.CloneAndOffset (pocicion.x, pocicion.y);      

		}

}
