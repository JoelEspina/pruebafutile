﻿using UnityEngine;
using System.Collections;

public class Pelota : DependesiaC
{
		public Pelota ()
		{
				Create ();
		}

		public override void Create ()
		{
				textura2D = new FSprite ("ball");
				pocicion = new Vector2 (320, 240);
				textura2D.x = pocicion.x;
				textura2D.y = pocicion.y; 
				AddChild (textura2D);
				velocidad = new Vector2 (5, -5);
				ActualizarTex ();


		}

		public override void Actualizar30 ()
		{
				pocicion.x += velocidad.x;
				pocicion.y += velocidad.y;
				ActualizarTex ();
		
				if (pocicion.y > Futile.screen.height) {
						velocidad.y = -velocidad.y;
				}
				if (pocicion.y < 0) {
						velocidad.y = -velocidad.y;
				}
				
		
				rectanguloColicion = textura2D.localRect.CloneAndOffset (pocicion.x, pocicion.y);      
		}
}
