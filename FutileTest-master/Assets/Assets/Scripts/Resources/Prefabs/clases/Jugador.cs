﻿using UnityEngine;
using System.Collections;

public class Jugador : DependesiaC
{
		public int puntos;
		public FLabel marcador;

		public Jugador ()
		{
				Create ();
		}

		public override void Create ()
		{
				textura2D = new FSprite ("p");
				rectanguloColicion = textura2D.localRect.CloneAndOffset (0, 0);
				ActualizarTex ();
				AddChild (textura2D);
		}

		public override void Actualizar30 ()
		{
				pocicion.y += velocidad.y;
				ActualizarTex ();
		}

}