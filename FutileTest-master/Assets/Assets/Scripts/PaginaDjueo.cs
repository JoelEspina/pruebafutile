﻿using UnityEngine;
using System.Collections;

public class PaginaDjueo :Page
{
		Pelota pelota;
		JugadorHumano jugadorhumano;
		JugadorCom jugadorcom;

		public PaginaDjueo ()
		{
				pelota = new Pelota ();
				jugadorhumano = new JugadorHumano ();
				jugadorcom = new JugadorCom (pelota);
				AddChild (pelota);
				AddChild (jugadorcom);
				AddChild (jugadorhumano);
				Start ();
		}

		override public void Start ()
		{
				ListenForUpdate (Actualizar30);
		}

		void Actualizar30 ()
		{
				if (pelota.rectanguloColicion.CheckIntersect (jugadorcom.rectanguloColicion)) {
						pelota.velocidad.x = -6;
						Debug.Log ("collison with compu");
				}

				if (pelota.rectanguloColicion.CheckIntersect (jugadorhumano.rectanguloColicion)) {
						pelota.velocidad.x = 6;
						Debug.Log ("collison with humano");
				}
				if (pelota.pocicion.x > Futile.screen.width) {
						jugadorhumano.puntos = jugadorhumano.puntos + 1;
						pelota.pocicion.x = Futile.screen.halfWidth;
				}
						
				if (pelota.pocicion.x < 0) {
						jugadorcom.puntos = jugadorcom.puntos + 1;
						pelota.pocicion.x = Futile.screen.halfWidth;
				}
		if( jugadorcom.puntos > 3)
		{
			Game.instance.EnviarAPagina(new MenuInisial());
		}

				
				pelota.Actualizar30 ();
				jugadorhumano.Actualizar30 ();
				jugadorcom.Actualizar30 ();

		}

}
