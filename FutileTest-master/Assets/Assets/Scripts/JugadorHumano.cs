﻿using UnityEngine;
using System.Collections;

public class JugadorHumano : Jugador
{       
		Pelota pelota;
		
		public JugadorHumano (Pelota _pelota):base()
		{
				pelota = _pelota;
		}

		public JugadorHumano ():base()
		{
		}

		public override void Create ()
		{
				pocicion = new Vector2 (50, 380);
				base.Create ();
				velocidad = new Vector2 (0, 8);
				marcador = new FLabel ("FontPuntos", "marcador"); //al varaible playerName le pasas el Nombre del Font y despues el texto
				marcador.SetPosition (50, 380); //Aqui podes ponerle posicion 
				AddChild (marcador);
		}

		public override void Actualizar30 ()
		{
				velocidad.y = 0;

				if (Input.GetKey (KeyCode.UpArrow)) {
						velocidad.y = 6;
				}

				if (Input.GetKey (KeyCode.DownArrow)) {
						velocidad.y = -6;
				}
				if (pocicion.y + velocidad.y > Futile.screen.height) {
						velocidad.y = 0;
				}
				if (pocicion.y + velocidad.y < 0) {
						velocidad.y = 0;
				}
				base.Actualizar30 ();

				marcador.text = puntos.ToString (); // aqui estoy cambiando el texto a que diga la velocidad del jugadorHumano

				//Tambien llam la funcion ToString de velodidad.y cual convierte un numero a una cadena

				marcador.SetPosition (pocicion.x, pocicion.y); // aqui estoy mandandole la pocicion del jugador al variable de FLabel que creamos arriba
		}
}	