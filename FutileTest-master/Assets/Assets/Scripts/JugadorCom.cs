﻿using UnityEngine;
using System.Collections;

public class JugadorCom : Jugador
{
		
		Pelota  pelota;

		public JugadorCom (Pelota _pelota):base()
		{
				pelota = _pelota;
		}

		public override void Create ()
		{
				pocicion = new Vector2 (610, 380);
				base.Create ();
				velocidad = new Vector2 (0, 8);
				marcador = new FLabel ("FontPuntos", "marcador"); //al varaible playerName le pasas el Nombre del Font y despues el texto
				marcador.SetPosition (610, 380); //Aqui podes ponerle posicion 
				AddChild (marcador);
		}

		public override void Actualizar30 ()
		{
				velocidad.y = pelota.velocidad.y;
				if (pocicion.y < 0) {
						velocidad.y = -velocidad.y;
				}
				if (pocicion.y > Futile.screen.height) {
						velocidad.y = -velocidad.y;
				}
				base.Actualizar30 ();
		marcador.text = puntos.ToString (); // aqui estoy cambiando el texto a que diga la velocidad del jugadorHumano
		
		//Tambien llam la funcion ToString de velodidad.y cual convierte un numero a una cadena
		
		marcador.SetPosition (pocicion.x, pocicion.y); 
		}

}
