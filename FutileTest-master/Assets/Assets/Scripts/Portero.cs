﻿using UnityEngine;
using System.Collections;

public class Portero : DependesiaC
{
		public int puntos;
		public FLabel marcador;
		Pelota pelota;
	
		public Portero (Pelota _pelota):base()
		{
				pelota = _pelota;
		
				Create ();
		}
	
		public Portero ():base()
		{
		
				Create ();
		}
	
		public override void Create ()
		{
				textura2D = new FSprite ("PlayerShip_0");
				pocicion = new Vector2 (320, 50);
				AddChild (textura2D);
				ActualizarTex ();
				velocidad = new Vector2 (0, 0);
				marcador = new FLabel ("FontPuntos", "marcador"); //al varaible playerName le pasas el Nombre del Font y despues el texto
				marcador.SetPosition (320, 50); //Aqui podes ponerle posicion 
				puntos = 1;		
				AddChild (marcador);
		}
	
		public override void Actualizar30 ()
		{
				pocicion.x += velocidad.x;
				ActualizarTex ();		

				velocidad.x = 0;
		
				if (Input.GetKey (KeyCode.RightArrow)) {
						velocidad.x = 8;
				}
		
				if (Input.GetKey (KeyCode.LeftArrow)) {
						velocidad.x = -8;
				}
				if (pocicion.x + velocidad.x > Futile.screen.width) {
						velocidad.x = 0;
				}
				if (pocicion.x + velocidad.x < 0) {
						velocidad.x = 0;
				}
		
				marcador.text = puntos.ToString (); // aqui estoy cambiando el texto a que diga la velocidad del jugadorHumano
		
				//Tambien llam la funcion ToString de velodidad.y cual convierte un numero a una cadena
		
				marcador.SetPosition (pocicion.x, pocicion.y); // aqui estoy mandandole la pocicion del jugador al variable de FLabel que creamos arriba
		
				
		}
}

