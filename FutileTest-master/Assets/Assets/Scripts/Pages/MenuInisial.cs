﻿using UnityEngine;
using System.Collections;

public class MenuInisial : Page {

	FButton botonPingball; 
	FButton botonBrickball;

	public MenuInisial()
	{
		botonPingball = new FButton ("ball");
		botonBrickball = new FButton ("PlayerShip_0");
		AddChild (botonPingball);
		AddChild (botonBrickball);
		botonBrickball.x = 240;
		botonBrickball.y = 220;
		botonPingball.x = 240;
		botonPingball.y = 420;
		botonPingball.AddLabel ("FontPuntos","                      pingball",Color.white);
		botonBrickball.AddLabel ("FontPuntos","                     Brickball",Color.white);
		botonPingball.SignalPress += BPingball;
		botonBrickball.SignalPress += BBrickball;
	}

	public void BPingball(FButton boton)
	{
		Game.instance.EnviarAPagina(new PaginaDjueo());
	}

	public void BBrickball(FButton boton)
	{
		Game.instance.EnviarAPagina(new Cuarto());
	}



}