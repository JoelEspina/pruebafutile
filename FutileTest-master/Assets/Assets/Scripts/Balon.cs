﻿using UnityEngine;
using System.Collections;

public class Balon : DependesiaC
{
		public Balon ()
		{
				Create ();
		}
	
		public override void Create ()
		{
				textura2D = new FSprite ("ball");
				pocicion = new Vector2 (320, 50);
				textura2D.x = pocicion.x;
				textura2D.y = pocicion.y; 
				AddChild (textura2D);
				velocidad = new Vector2 (4, -4);
				ActualizarTex ();
		
		
		}
	
		public override void Actualizar30 ()
		{
				pocicion.x += velocidad.x;
				pocicion.y += velocidad.y;
				ActualizarTex ();
		
				if (pocicion.x > Futile.screen.width) {
						velocidad.x = -velocidad.x;
				}
				if (pocicion.x < 0) {
						velocidad.x = -velocidad.x;
				}

				if (pocicion.y > Futile.screen.height) {
						velocidad.y = -velocidad.y;
				}

				
		
				rectanguloColicion = textura2D.localRect.CloneAndOffset (pocicion.x, pocicion.y);      
		}
}



